package bogdan.moviesapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Bogdan on 1/7/2017.
 */

public class SaveStatus {

    private static final String PREF_USER_EMAIL= "username";
    private static final String PREF_USER_FIRST_NAME = "first_name";
    private static final String PREF_USER_LAST_NAME = "last_name";
    private static final String PREF_USER_PROFILE_PIC = "profilepic";
    private static final String PREF_USER_FB_ID = "fb_id";


    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void logOut(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear().apply();
    }

    public static void setUserId(Context ctx, String id)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_FB_ID,id);
        editor.apply();
    }

    public static void setUserFirstName(Context ctx, String fName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_FIRST_NAME, fName);
        editor.apply();
    }

    public static void setUserLastName(Context ctx, String lName){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_LAST_NAME, lName);
        editor.apply();
    }

    public static void setUserEmail(Context ctx, String email)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_EMAIL, email);
        editor.apply();
    }

    public static void setUserProfilePic(Context ctx, String picAdress) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_PROFILE_PIC, picAdress);
        editor.apply();
    }

    public static String getUserId(Context ctx)  {
        return getSharedPreferences(ctx).getString(PREF_USER_FB_ID, "");
    }

    public static String getUserFirstName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_FIRST_NAME, "");
    }

    public static String getUserLasttName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_LAST_NAME, "");
    }

    public static String getUserEmail(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_EMAIL, "");
    }

    public static String getUserProfilePic (Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_PROFILE_PIC,"");
    }


}
