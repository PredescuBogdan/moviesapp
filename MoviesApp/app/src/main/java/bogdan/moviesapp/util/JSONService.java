package bogdan.moviesapp.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bogdan.moviesapp.model.Actor;
import bogdan.moviesapp.model.Cinema;
import bogdan.moviesapp.model.Ecranizare;
import bogdan.moviesapp.model.Movie;
import bogdan.moviesapp.model.Premiu;

/**
 * Created by Bogdan on 1/15/2017.
 */

public class JSONService {


    public static List<Movie> topMovieToList(String json, String type){
        List<Movie> movies = new ArrayList<Movie>();
        try {
            JSONArray jsonA = new JSONArray(json);

            if(type.equals("withActors")) {
                for (int i = 0; i < jsonA.length(); i++) {
                    JSONObject obj = jsonA.getJSONObject(i);
                    String name = obj.getString("name");
                    String releaseDate = obj.getString("release_date");
                    String description = obj.getString("description");
                    String actors = obj.getString("actors");
                    String cover = obj.getString("cover");
                    Movie movie = new Movie(name, releaseDate, description, actors,cover);
                    movies.add(movie);
                }

            }else {
                for (int i = 0; i < jsonA.length(); i++) {
                    JSONObject obj = jsonA.getJSONObject(i);
                    String name = obj.getString("name");
                    String releaseDate = obj.getString("release_date");
                    Movie movie = new Movie(name, releaseDate);
                    movies.add(movie);
                }
            }

            }catch(Exception e){
            e.printStackTrace();
        }
        return movies;
    }

    public static List<Actor> actorsToList(String json){
        List<Actor> actors = new ArrayList<Actor>();
        try {
            JSONArray jsonA = new JSONArray(json);

           {
                for (int i = 0; i < jsonA.length(); i++) {
                    JSONObject obj = jsonA.getJSONObject(i);
                    String firstName = obj.getString("firstName");
                    String lastName = obj.getString("lastName");
                    String movies = obj.getString("moviesPlayed");
                    Actor actor = new Actor(firstName,lastName,movies);
                    actors.add(actor);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return actors;
    }

    public static List<Premiu> premiiToList(String json){
        List<Premiu> premii = new ArrayList<>();
        try {
            JSONArray jsonA = new JSONArray(json);

            {
                for (int i = 0; i < jsonA.length(); i++) {
                    JSONObject obj = jsonA.getJSONObject(i);
                    String name = obj.getString("nume");
                    Premiu premiu = new Premiu(name);
                    premii.add(premiu);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return premii;
    }


    public static List<Cinema> cinematografeToList(String json){
        List<Cinema> cinematografe = new ArrayList<>();
        try {
            JSONArray jsonA = new JSONArray(json);

            {
                for (int i = 0; i < jsonA.length(); i++) {
                    JSONObject obj = jsonA.getJSONObject(i);
                    String name = obj.getString("nume");
                    String adresa = obj.getString("adresa");
                    int id = obj.getInt("id");
                    Cinema cinema = new Cinema(id, name, adresa);
                    cinematografe.add(cinema);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return cinematografe;
    }


    public static List<Ecranizare> ecranizariiToList(String json){
        List<Ecranizare> ecranizarii = new ArrayList<>();
        try {
            JSONArray jsonA = new JSONArray(json);

            {
                for (int i = 0; i < jsonA.length(); i++) {
                    JSONObject obj = jsonA.getJSONObject(i);
                    String cinemaName = obj.getString("cinemaName");
                    String date = obj.getString("releaseDate");
                    String details = obj.getString("details");
                    Ecranizare ecranizare = new Ecranizare(date, details, cinemaName);
                    ecranizarii.add(ecranizare);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return ecranizarii;
    }
}
