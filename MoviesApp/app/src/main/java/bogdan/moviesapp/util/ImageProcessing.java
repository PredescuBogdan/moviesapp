package bogdan.moviesapp.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Bogdan on 1/8/2017.
 */

public class ImageProcessing extends AsyncTask<String,Void,Void>{

    private Context context;

    public void setContext(Context ctx) {
        this.context=ctx;
    }

    private static String encodeImage(String url) {
        Bitmap realImage = getBitmapFromURL(url);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);

    }


    private static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }


    @Override
    protected Void doInBackground(String...Params) {
        String imagineEncodata = encodeImage(Params[0]);
        SaveStatus.setUserProfilePic(context,imagineEncodata);
        return null;
    }


    @Override
    protected void onPostExecute(Void o) {
        super.onPostExecute(o);
    }
}
