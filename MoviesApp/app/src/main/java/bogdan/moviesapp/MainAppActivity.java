package bogdan.moviesapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import bogdan.moviesapp.adapter.PagerTabsAdapter;
import bogdan.moviesapp.util.SaveStatus;

public class MainAppActivity extends AppCompatActivity {

    PagerTabsAdapter adapter;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(SaveStatus.getUserEmail(getApplicationContext()).length() != 0) {
            setContentView(R.layout.activity_main_app);
//            new DAOUser(new User("predescu.bogdan93@gmail.com", "professional93")).execute("getAccessUserNormal");
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);



            tabLayout = (TabLayout) findViewById(R.id.tab_layout);
            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.newicon));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.movie));
//        tabLayout.addTab(tabLayout.newTab().setText("Extragere"));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.actor));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.award));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.cinema));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            adapter = new PagerTabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });


        } else {
            Intent i = new Intent(MainAppActivity.this,WelcomeActivity.class);
            startActivity(i);
        }

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.item1 :
                Log.d("DEBUG : ", "APASAT OPTIONS MENU");
                Intent i = new Intent(MainAppActivity.this,AccountSettingsActivity.class);
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
