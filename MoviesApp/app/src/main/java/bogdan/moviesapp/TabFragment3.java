package bogdan.moviesapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import bogdan.moviesapp.adapter.PremiuAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Premiu;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class TabFragment3 extends Fragment {

    ListView listaPremii;
    Context context;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaPremii = (ListView) view.findViewById(R.id.listaPremii);
        context = view.getContext();
        new IncarcatorDeJsoane().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_3, container, false);
    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(iConstants.SERVER_URL + "/findPremii");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Premiu> premii = JSONService.premiiToList(s);
            final PremiuAdapter adaptor = new PremiuAdapter(context, premii);

            listaPremii.setAdapter(adaptor);
            listaPremii.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent premiuDetailsActivity = new Intent(getContext(), PremiuDetailsActivity.class);
                    premiuDetailsActivity.putExtra("Nume", premii.get(i).getNume());
                    startActivity(premiuDetailsActivity);
                }
            });

            super.onPostExecute(s);
        }
    }
}
