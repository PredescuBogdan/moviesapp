package bogdan.moviesapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import bogdan.moviesapp.adapter.CinemaAdapter;
import bogdan.moviesapp.adapter.PremiuAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Cinema;
import bogdan.moviesapp.model.Premiu;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;

public class TabFragment5 extends Fragment {

    ListView listaCinematografe;
    Context context;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        context = view.getContext();
        listaCinematografe = (ListView) view.findViewById(R.id.listViewCinematografe);
        new IncarcatorDeJsoane().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_5, container, false);
    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(iConstants.SERVER_URL + "/cinematografe/all");
            Log.d("CINEMAS: ", json);
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Cinema> cinematografe = JSONService.cinematografeToList(s);
            final CinemaAdapter adaptor = new CinemaAdapter(context, cinematografe);

            listaCinematografe.setAdapter(adaptor);

            listaCinematografe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Cinema cinema = cinematografe.get(i);
                    Intent it = new Intent(getContext(), CinematografeMapsActivity.class);
                    it.putExtra("ADRESA", cinema.getAdresa());
                    it.putExtra("NUME_CINEMA", cinema.getNume());
                    startActivity(it);
                }
            });

            super.onPostExecute(s);
        }
    }
}
