package bogdan.moviesapp;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class CinematografeMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public LatLng getLocationFromAddress(Context context, String strAddress)
    {
        Geocoder coder= new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try
        {
            address = coder.getFromLocationName(strAddress, 5);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return p1;

    }


    private GoogleMap mMap;
    private String adresa = null;
    private String numeCinema = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinematografe_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        adresa = getIntent().getExtras().getString("ADRESA");
        numeCinema = getIntent().getExtras().getString("NUME_CINEMA");

        Log.d("ADRESA: ", adresa);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng pos = getLocationFromAddress(this, adresa); //new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(pos).title(numeCinema));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(pos), 12.0f);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 12.0f));

    }
}
