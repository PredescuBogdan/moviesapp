package bogdan.moviesapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import bogdan.moviesapp.adapter.PremiuDetailsMovieAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Movie;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class PremiuDetailsActivity extends AppCompatActivity {
    @InjectView(R.id.listViewPremiiDetails)
    ListView listaPremiiAndFilme;
    String numePremiu = null;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premiu_details);
        context = this;
        Intent i = getIntent();
        numePremiu = i.getExtras().getString("Nume");
        ButterKnife.inject(this);
        new IncarcatorDeJsoane().execute();

    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();

            String json = handler.makeServiceCall(iConstants.SERVER_URL + "/findAllMoviesByPremiu/"+numePremiu);
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Movie> movies = JSONService.topMovieToList(s, "noActors");
            final PremiuDetailsMovieAdapter adaptor = new PremiuDetailsMovieAdapter(context, movies);

            listaPremiiAndFilme.setAdapter(adaptor);
            super.onPostExecute(s);
        }
    }
}
