package bogdan.moviesapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import bogdan.moviesapp.adapter.MoviesAdapter;
import bogdan.moviesapp.adapter.NewMoviesAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Movie;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;

public class TabFragment4 extends Fragment {


    private ListView listaFilme;
    private EditText searchMessage;
    private Context context;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaFilme = (ListView) view.findViewById(R.id.listViewFilme);
        searchMessage = (EditText)view.findViewById(R.id.inputSearch);
        context = view.getContext();
       IncarcatorDeJsoane incarcatorDeJsoane = new IncarcatorDeJsoane();
        incarcatorDeJsoane.execute();

    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(iConstants.SERVER_URL+"/getAllMoviesWithActors/200");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Movie> movies = JSONService.topMovieToList(s, "withActors");
            final MoviesAdapter adaptor = new MoviesAdapter(context, movies);
//            ArrayAdapter<String> itemsAdapter =
//                    new ArrayAdapter<String>(ReviewsActivity.this, android.R.layout.simple_list_item_1, reviews);

            searchMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    adaptor.getFilter().filter(charSequence);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            listaFilme.setAdapter(adaptor);
            listaFilme.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent movieDetails = new Intent(getContext(), MovieDetailsActvity.class);
                    movieDetails.putExtra("Position", i);
                    Movie mov = movies.get(i);
                    mov.setCover(null);
                    movieDetails.putExtra("Movie", movies.get(i));
                    startActivity(movieDetails);
                }
            });
            super.onPostExecute(s);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.tab_fragment_4, container, false);
    }
}
