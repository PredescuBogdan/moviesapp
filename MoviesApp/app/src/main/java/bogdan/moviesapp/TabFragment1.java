package bogdan.moviesapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import bogdan.moviesapp.adapter.NewMoviesAdapter;
import bogdan.moviesapp.model.Movie;
import bogdan.moviesapp.util.ImageProcessing;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.SaveStatus;
import bogdan.moviesapp.util.iConstants;

import bogdan.moviesapp.http.HttpHandler;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class TabFragment1 extends Fragment {

    private ListView listaFilme;
    Context context;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("DEBUG", "FG 1 CREATED");
        listaFilme = (ListView) view.findViewById(R.id.listViewFilme);
        context = view.getContext();
        IncarcatorDeJsoane incarcatorDeJsoane = new IncarcatorDeJsoane();
        incarcatorDeJsoane.execute();
    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(iConstants.SERVER_URL + "/getAllMoviesWithActors/10");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Movie> movies = JSONService.topMovieToList(s, "withActors");
            final NewMoviesAdapter adaptor = new NewMoviesAdapter(context, movies);

            listaFilme.setAdapter(adaptor);
            listaFilme.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent movieDetails = new Intent(getContext(), MovieDetailsActvity.class);
                    movieDetails.putExtra("Position", i);
                    Movie mov = movies.get(i);
                    mov.setCover(null);
                    movieDetails.putExtra("Movie", movies.get(i));
                    startActivity(movieDetails);
                }
            });
            super.onPostExecute(s);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_1, container, false);
    }
}

