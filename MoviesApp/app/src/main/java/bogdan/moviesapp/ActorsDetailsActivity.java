package bogdan.moviesapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

import bogdan.moviesapp.adapter.ActorsAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Actor;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class ActorsDetailsActivity extends AppCompatActivity {

    @InjectView(R.id.actorsDetailsFirstName)TextView actorFirstName;
    @InjectView(R.id.actorsDetailsLastName)TextView actorLastName;
    @InjectView(R.id.actorsDetailsMovies)TextView moviesPlayed;

    int pos = 0;
    ActorsAdapter adaptor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actors_details);
        ButterKnife.inject(this);

        Intent i = getIntent();
        pos = i.getExtras().getInt("Position");

        new IncarcatorDeJsoane().execute();

    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(iConstants.SERVER_URL+"/getAllActorsWithMovies");
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Actor> movies = JSONService.actorsToList(s);
            adaptor = new ActorsAdapter(getApplicationContext(), movies);


            Actor actor =  adaptor.getItem(pos);

            Log.d("DEBUG EXTRA NUME", actor.getFirstName());

            actorFirstName.setText("First Name:\n" +  actor.getFirstName());
            actorLastName.setText("Last Name: \n" +actor.getLastName());
            moviesPlayed.setText("Movies Played :\n" + actor.getMoviesPlayed());


//            ArrayAdapter<String> itemsAdapter =
//                    new ArrayAdapter<String>(ReviewsActivity.this, android.R.layout.simple_list_item_1, reviews);
            super.onPostExecute(s);
        }
    }

}
