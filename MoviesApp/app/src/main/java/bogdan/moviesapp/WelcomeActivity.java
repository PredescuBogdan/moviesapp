package bogdan.moviesapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Arrays;

import bogdan.moviesapp.dao.DAOUser;
import bogdan.moviesapp.model.User;
import bogdan.moviesapp.util.CryptWithMD5;
import bogdan.moviesapp.util.GetFacebookData;
import bogdan.moviesapp.util.ImageProcessing;
import bogdan.moviesapp.util.SaveStatus;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class WelcomeActivity extends Activity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @InjectView(R.id.input_email)
    EditText _emailText;
    @InjectView(R.id.input_password)
    EditText _passwordText;
    @InjectView(R.id.btn_login)
    Button _loginButton;
    @InjectView(R.id.link_signup)
    TextView _signupLink;
    @InjectView(R.id.btn_loginfb)
    LoginButton _loginFbButton;

    private CallbackManager callbackManager;
    private Bundle bFacebookData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(SaveStatus.getUserEmail(getApplicationContext()).length() == 0) {
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager = CallbackManager.Factory.create();
            setContentView(R.layout.activity_welcome);
            ButterKnife.inject(this);

            _loginFbButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    _loginFbButton.setReadPermissions(Arrays.asList(
                            "public_profile", "email"));
                    _loginFbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {

                            System.out.println("onSuccess");
                            String accessToken = loginResult.getAccessToken().getToken();
                            Log.i("accessToken", accessToken);
                            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    Log.i("LoginActivity", response.toString());
                                    // Get facebook data from login
                                    bFacebookData = GetFacebookData.getFacebookData(object);
                                    Context context = getApplicationContext();
                                    User userFacebook = new User(new BigInteger(bFacebookData.getString("idFacebook")),bFacebookData.getString("email"));

                                    boolean access = false;
                                    try{
                                        access = new DAOUser(userFacebook).execute("UserFacebook").get();
                                    } catch(Exception e) {
                                        e.printStackTrace();
                                    }

                                    if(access) {
                                        Log.d("DEBUG FB: ", " MATCH LA FB");
                                        finish();
                                        SaveStatus.setUserEmail(context, bFacebookData.getString("email"));
                                        SaveStatus.setUserFirstName(context, bFacebookData.getString("first_name"));
                                        SaveStatus.setUserLastName(context, bFacebookData.getString("last_name"));
//                                    SaveStatus.setUserProfilePic(context, bFacebookData.getString("profile_pic"));
                                        SaveStatus.setUserId(context,bFacebookData.getString("idFacebook"));

                                        ImageProcessing imgProc = new ImageProcessing();

                                        imgProc.setContext(getApplicationContext());
                                        imgProc.execute(bFacebookData.getString("profile_pic"));
                                        nextActivity();
                                    } else {
                                        FacebookSdk.sdkInitialize(getApplicationContext());
                                        LoginManager.getInstance().logOut();
                                        onLoginFailed();
                                    }

                                }
                            });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            System.out.println("onCancel");
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            System.out.println("onError");
                            Log.v("LoginActivity", exception.getCause().toString());
                        }
                    });


                }
            });

            _loginButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    login();
                }
            });

            _signupLink.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // Start the Signup activity
                    Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                    startActivityForResult(intent, REQUEST_SIGNUP);

                }
            });
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();

        }else {
            nextActivity();
        }
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(WelcomeActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        final String email = _emailText.getText().toString();
        final String password = CryptWithMD5.cryptWithMD5(_passwordText.getText().toString());

        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
//                        if(_passwordText.getText().toString().equals("prof")) {

                        User u = new User(email,password);
                        Log.d("DEBUG USER", u.getPassword());
                        Boolean access = false;
                        try {
                            access= new DAOUser(u).execute("UserNormal").get();
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        if(access)  {
                            Log.d("DEBUG ACCESS: ", "Avem access");
                            onLoginSuccess();
                        }else {
                            onLoginFailed();
                        }

                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    //Ce se intampla dupa ce primim datele de la register -> TODO: send to db, open new intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_SIGNUP) {
//            if (resultCode == RESULT_OK) {

        // TODO: Implement successful signup logic here
        // By default we just finish the Activity and log them in automatically


//            }
//        }
    }

//    @Override
//    public void onBackPressed() {
//        // disable going back to the MainActivity
//        moveTaskToBack(false);
//    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);

        SaveStatus.setUserEmail(getApplicationContext(),_emailText.getText().toString());
        nextActivity();
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 18) {
            _passwordText.setError("between 4 and 18 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;

    }

    public void nextActivity() {
        Intent i = new Intent(WelcomeActivity.this, MainAppActivity.class);
        startActivity(i);
    }
}
