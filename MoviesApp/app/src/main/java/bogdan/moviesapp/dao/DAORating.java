package bogdan.moviesapp.dao;

import android.os.AsyncTask;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;
import bogdan.moviesapp.model.Rating;
import bogdan.moviesapp.util.iConstants;

/**
 * Created by Bogdan on 1/19/2017.
 */

public class DAORating extends AsyncTask<Void,Void,Void> {

    private Rating r;

    public DAORating(Rating r) {
        this.r = r;
    }

    @Override
    protected Void doInBackground(Void... params) {

     getRating();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }


    public void getRating () {

        RestTemplate restTemplate = new RestTemplate();
        String url = iConstants.SERVER_URL + "/putdata/{email_user}/{film_title}?rating="+ r.getRating();
        Map<String, String> map = new HashMap<String, String>();
        map.put("email_user", r.getEmail());
        map.put("film_title",r.getTitle());
        restTemplate.put(url, null, map);

    }
}