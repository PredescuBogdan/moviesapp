package bogdan.moviesapp.dao;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import bogdan.moviesapp.model.Message;
import bogdan.moviesapp.model.User;
import bogdan.moviesapp.util.iConstants;

/**
 * Created by Bogdan on 1/9/2017.
 */

public class DAOUser extends AsyncTask<String,Void,Boolean>{

    private User u;
    private boolean access = false;

    public DAOUser(User u) {
        this.u = u;
    }

    @Override
    protected Boolean doInBackground(String... params) {

        if(params[0].equals("UserNormal")) {
            getAccessUserNormal();
        }else if(params[0].equals("UserFacebook")) {
            getAccessUserFacebook();
        }
        return access;
    }

    @Override
    protected void onPostExecute(Boolean aVoid) {
        access = false;
        super.onPostExecute(aVoid);
    }

    private boolean getAccessUserNormal() {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        String url = iConstants.SERVER_URL + "/findNormalUser";
        User userNormal = u;
        Message message= restTemplate.postForObject(url, userNormal, Message.class);
        Log.d("DEBUG USER TRIMIS: " , userNormal.getPassword());
        System.out.println(message.getMessage());

        if(message.isAccess()) {
          access = true;
        }

        return message.isAccess();
    }


    private boolean getAccessUserFacebook() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        String url = iConstants.SERVER_URL + "/findUserFacebook";
        User userFacebook = u;
        Message message= restTemplate.postForObject(url, userFacebook, Message.class);
//        Log.d("DEBUG USER TRIMIS: " , userFacebook.getPassword());
        System.out.println(message.getMessage());

        if(message.isAccess()) {
            access = true;
        }

        return message.isAccess();
    }


    //    private static void getUserAccess () {
//
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//        String url = "http://192.168.1.64:9080/__MoviesServerREST/findUser";
//        User user = new User("predescu.bogdan93@gmail.com", new Date());
//        Message message= restTemplate.postForObject(url, user, Message.class);
//        System.out.println(message.getMessage());
//        Log.d("DEBUG FROM SV: ","Message content: " +  message.getMessage());
//        Log.d("DEBUG FROM SV: ", "Access for " + user.getEmail() + ": " + message.isAccess());
//    }


//    private static void getMessage()
//    {
//        final String uri = "http://192.168.1.64:9080/__MoviesServerRest/getAccess/";
//
//
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//        Message message = restTemplate.getForObject("http://192.168.1.64:9080/__MoviesServerREST/getAccess/{id}", Message.class, 1);
//        System.out.println("Message: " + message.getMessage());
//        System.out.println("Access: " + message.isAccess());
//    }

//    private static void getEmployees()
//    {
//        final String uri = "http://192.168.1.64:9080/__PolisengerServerREST/getAndroid";
//
//
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//        Person person = restTemplate.getForObject("http://192.168.1.64:9080/__PolisengerServerREST/fetchjson/{id}", Person.class, 200);
//        System.out.println("ID: " + person.getId());
//        System.out.println("Name: " + person.getName());
//        System.out.println("Village Name: " + person.getAddress().getVillage());
//    }



}
