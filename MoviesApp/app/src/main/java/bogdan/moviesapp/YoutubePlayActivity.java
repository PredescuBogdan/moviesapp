package bogdan.moviesapp;

import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.util.iConstants;

public class YoutubePlayActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_DIALOG_REQUEST = 1;

    // YouTube player view
    private YouTubePlayerView youTubeView;
    String trailerLink = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new IncarcatorDeJsoane().execute();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_youtube_play);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

//        youTubeView = new YouTubePlayerView(this);
//        youTubeView.inflate(this, R.layout.activity_youtube_play,R.id.);


        // Initializing video player with developer key
        youTubeView.initialize(Config.DEVELOPER_KEY, this);

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = "ERROR";
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
//            player.setFullscreen(true);
            player.loadVideo(trailerLink.trim());
            player.setFullscreen(false);
            player.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                @Override
                public void onFullscreen(boolean b) {

                }
            });
            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically


            // Hiding player controls
//            player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            Intent i = getIntent();
            String title = i.getExtras().getString("movieTitle").replace("?","%3F");


            String trailer = handler.makeServiceCall(iConstants.SERVER_URL + "/getTrailerByMovie/" + title);
            Log.d("DEBUG FILM NAME :", i.getExtras().getString("movieTitle"));
            Log.d("TR LK YU: ", trailer);
            return trailer;
        }
        @Override
        protected void onPostExecute(String s) {

            trailerLink = s;
            Log.d("TR LK YU: ", s);
            super.onPostExecute(s);
        }
    }
}