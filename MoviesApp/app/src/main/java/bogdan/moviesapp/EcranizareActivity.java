package bogdan.moviesapp;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import bogdan.moviesapp.adapter.EcranizareAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Ecranizare;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;

public class EcranizareActivity extends AppCompatActivity {

    ListView listaEcranizari;
    Context context;
    String movieTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecranizare);
        context = getApplicationContext();
        listaEcranizari = (ListView) findViewById(R.id.listViewEcranizari);
        new IncarcatorDeJsoane().execute();
    }


    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            Intent i = getIntent();
            movieTitle = i.getExtras().getString("movieTitle");
            String json = handler.makeServiceCall(iConstants.SERVER_URL+"/cinematografe/ecranizareMovie/"+movieTitle.replace("?","%3F"));
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            final List<Ecranizare> ecranizari = JSONService.ecranizariiToList(s);

            try {
                int nr = ecranizari.get(0).getCinemaName().length();

            }catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                Toast.makeText(context,"Movie not available in cinemas",Toast.LENGTH_SHORT).show();
                finish();
            }


            EcranizareAdapter adaptor = new EcranizareAdapter(context, ecranizari);
            listaEcranizari.setAdapter(adaptor);
            super.onPostExecute(s);
        }
    }
}
