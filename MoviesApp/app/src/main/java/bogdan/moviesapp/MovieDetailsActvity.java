package bogdan.moviesapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;

import bogdan.moviesapp.adapter.NewMoviesAdapter;
import bogdan.moviesapp.dao.DAORating;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Ecranizare;
import bogdan.moviesapp.model.Movie;
import bogdan.moviesapp.model.Rating;
import bogdan.moviesapp.util.ImageProcessing;
import bogdan.moviesapp.util.SaveStatus;
import bogdan.moviesapp.util.iConstants;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class MovieDetailsActvity extends AppCompatActivity {

    @InjectView(R.id.movieDetailsTitle)TextView movieTitle;
    @InjectView(R.id.movieDetailsReleaseYear)TextView movieDate;
    @InjectView(R.id.movieDetailsDescription)TextView movieDescription;
    @InjectView(R.id.movieDetailsActors)TextView movieActors;
    @InjectView(R.id.movieCover) ImageView movieCover;
    @InjectView(R.id.buttonTrailer) Button butonTrailer;
    @InjectView(R.id.movieDetailsRating) TextView ratingTotal;
    @InjectView(R.id.buttonEcranizare) Button buttonEcranizare;

    int pos = 0;
    String cover = null;
    Button button;
    Movie movie = null;
    RatingBar ratingBar;
    private double rating;
    Activity activity;
    String ratingAVG = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        pos = i.getExtras().getInt("Position");
        final Movie movie = i.getExtras().getParcelable("Movie");
        this.movie = movie;
        new IncarcatorDeJsoane().execute();
        activity = this;

    }

    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
                HttpHandler handler = new HttpHandler();
                cover = handler.makeServiceCall(iConstants.SERVER_URL + "/getCoverByMovie/" + movie.getName().replace("?","%3F"));
                ratingAVG = handler.makeServiceCall(iConstants.SERVER_URL + "/getRatingForMovie/" + movie.getName().replace("?","%3F"));

            return cover;
        }
        @Override
        protected void onPostExecute(String s) {
            Log.d("DEBUG: ", "AM AJUNS PE EXTRA");
            setContentView(R.layout.activity_movie_details_actvity);
            ButterKnife.inject(activity);
            movieCover.setVisibility(View.INVISIBLE);
            movie.setCover(s);

            try {
                if(movie.getCover() == null || movie.getCover().equals("null")) {
                    movieCover.setVisibility(View.INVISIBLE);

                } else {
                    movieCover.setVisibility(View.VISIBLE);
                    TableRow.LayoutParams parms = new TableRow.LayoutParams(1000, 1000);
                    movieCover.setLayoutParams(parms);
                    movieCover.setImageBitmap(ImageProcessing.decodeBase64(movie.getCover()));
                }
            }catch (Exception e) {
                e.printStackTrace();
            }

            button = (Button)findViewById(R.id.buttonRating);
            ratingBar = (RatingBar)findViewById(R.id.ratingBar);
            movieTitle.setText("TITLE:\n" +  movie.getName());
            movieDate.setText("RELEASE DATE: \n" +movie.getRelease_date());
            movieDescription.setText("DESCRIPTION:\n" + movie.getDescription());
            movieActors.setText("ACTORS:\n" + movie.getActors());



            String rat = String.format(getResources().getString(R.string.rating), ratingAVG);
            ratingTotal.setText(rat);

            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    rating = v;
                }
            });

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("DEBUG: ", "CLICK");


                    try {
                        Rating rat = new Rating(SaveStatus.getUserEmail(getApplicationContext()), movie.getName(), rating);
                        Log.d("DEBUG DOUBLE: " ,String.valueOf(rat.getRating()));
                        Toast.makeText(getApplicationContext(), "Rated with " + rating + " stars ", Toast.LENGTH_SHORT).show();
                        DAORating daoRating = new DAORating(rat);
                        daoRating.execute();
                    }catch(Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            butonTrailer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MovieDetailsActvity.this, YoutubePlayActivity.class);
                    i.putExtra("movieTitle", movie.getName());
                    startActivity(i);

                }
            });


            buttonEcranizare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MovieDetailsActvity.this, EcranizareActivity.class);
                    i.putExtra("movieTitle", movie.getName());
                    startActivity(i);
                }
            });

            super.onPostExecute(s);
        }
    }
}
