package bogdan.moviesapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import bogdan.moviesapp.util.ImageProcessing;
import bogdan.moviesapp.util.SaveStatus;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

public class AccountSettingsActivity extends AppCompatActivity {

    @InjectView(R.id.textViewEmail)
    TextView email;

    @InjectView(R.id.buttonLogOutApp)
    Button buttonLogOutApp;

    @Optional
    @InjectView(R.id.imageViewProfilePic)
    ImageView profilePic;

    @Optional
    @InjectView(R.id.textViewFullName)
    TextView fullName;

    @Optional
    @InjectView(R.id.textViewFacebookId)
    TextView fbId;

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        String urlImg = SaveStatus.getUserProfilePic(getApplicationContext());
        new waitforImage().execute(urlImg);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = SaveStatus.getSharedPreferences(getApplicationContext());
        boolean previouslyStarted = prefs.getBoolean(getString(R.string.pref_previously_started), false);
        if(!previouslyStarted) {
            Log.d("DEBUG: ", "FIRST TIME STARTED");
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
            edit.apply();
        }
    }

     class waitforImage extends AsyncTask<String, Void, Bitmap> {
         ProgressDialog progressDialog;

         @Override
         protected void onPreExecute() {
             progressDialog  = new ProgressDialog(activity,
                     R.style.AppTheme_Dark_Dialog);
             progressDialog.setIndeterminate(true);
             progressDialog.show();
             super.onPreExecute();
         }

         protected Bitmap doInBackground(String... urls) {
             return ImageProcessing.decodeBase64(urls[0]);
         }


        protected void onPostExecute(Bitmap result) {
            progressDialog.dismiss();
            setContentView(R.layout.activity_account_settings);
            ButterKnife.inject(activity);
            if(SaveStatus.getUserProfilePic(getApplicationContext()).length() != 0) {
                profilePic.setImageBitmap(result);
            }


            buttonLogOutApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    LoginManager.getInstance().logOut();
                    SaveStatus.logOut(getApplicationContext());
                    Intent i = new Intent(AccountSettingsActivity.this,WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }
            });



            String userEmail = SaveStatus.getUserEmail(getApplicationContext());
            String fullNameFromPrefs = SaveStatus.getUserFirstName(getApplicationContext()) + SaveStatus.getUserLasttName(getApplicationContext());
            String facebookId = SaveStatus.getUserId(getApplicationContext());


            if(SaveStatus.getUserEmail(getApplicationContext()).length() != 0) {
                email.setText(getString(R.string.profile_email )+ userEmail );
            }


            if (SaveStatus.getUserFirstName(getApplicationContext()).length() != 0 && SaveStatus.getUserLasttName(getApplicationContext()).length() != 0) {
                fullName.setText(fullNameFromPrefs);
            }

            if(SaveStatus.getUserId(getApplicationContext()).length() != 0) {
                fbId.setText("Facebook ID: " + facebookId);
            }
        }
    }

}
