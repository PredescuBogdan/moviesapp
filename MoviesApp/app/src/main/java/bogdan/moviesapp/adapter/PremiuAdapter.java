package bogdan.moviesapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bogdan.moviesapp.R;
import bogdan.moviesapp.model.Premiu;

/**
 * Created by Bogdan on 1/24/2017.
 */

public class PremiuAdapter extends BaseAdapter{

    private Context context;
    private List<Premiu> listaPremii;

    public PremiuAdapter(Context context, List<Premiu> data) {
        this.context = context;
        this.listaPremii = data;
    }


    @Override
    public int getCount() {
        return listaPremii.size();
    }

    @Override
    public Premiu getItem(int i) {
        return listaPremii.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Premiu premiu = listaPremii.get(i);

        if(view == null) {
            view = inflater.inflate(R.layout.premiu_item, viewGroup,false);
        }



        TextView premiuName = (TextView) view.findViewById(R.id.premiuName);
        ImageView imaginePremiu = (ImageView)view.findViewById(R.id.imageViewPremiu);

        if(premiu.getNume().equals("golden globe")) {
            imaginePremiu.setImageResource(R.mipmap.goldenglobe);
        }else if (premiu.getNume().equals("oscar")) {
            imaginePremiu.setImageResource(R.mipmap.oscar);
        }else{
            imaginePremiu.setVisibility(View.GONE);
        }


        premiuName.setText(premiu.getNume());
        return view;
    }
}
