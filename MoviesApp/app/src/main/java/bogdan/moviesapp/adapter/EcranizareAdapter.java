package bogdan.moviesapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bogdan.moviesapp.R;
import bogdan.moviesapp.model.Ecranizare;
import bogdan.moviesapp.model.Premiu;

/**
 * Created by Bogdan on 1/24/2017.
 */

public class EcranizareAdapter extends BaseAdapter{

    private Context context;
    private List<Ecranizare> listaEcranizari;

    public EcranizareAdapter(Context context, List<Ecranizare> data) {
        this.context = context;
        this.listaEcranizari = data;
    }


    @Override
    public int getCount() {
        return listaEcranizari.size();
    }

    @Override
    public Ecranizare getItem(int i) {
        return listaEcranizari.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Ecranizare ecranizare = listaEcranizari.get(i);

        if(view == null) {
            view = inflater.inflate(R.layout.ecranizare_item, viewGroup,false);
        }



        TextView ecranizareCinema = (TextView) view.findViewById(R.id.ecranizareCinema);
        TextView ecranizareData = (TextView) view.findViewById(R.id.ecranizareData);
        TextView ecranizareSala = (TextView) view.findViewById(R.id.ecranizareSala);

        ecranizareCinema.setText(ecranizare.getCinemaName());
        ecranizareData.setText(ecranizare.getReleaseDate());
        ecranizareSala.setText(ecranizare.getDetails());

        return view;
    }
}
