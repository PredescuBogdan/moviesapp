package bogdan.moviesapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import bogdan.moviesapp.TabFragment1;
import bogdan.moviesapp.TabFragment2;
import bogdan.moviesapp.TabFragment3;
import bogdan.moviesapp.TabFragment4;
import bogdan.moviesapp.TabFragment5;


public class PagerTabsAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;


    public PagerTabsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragment1 tab1 = new TabFragment1();
                return tab1;
            case 1:
                TabFragment4 tab2 = new TabFragment4();
                return tab2;
            case 2:
                TabFragment2 tab3 = new TabFragment2();
                return tab3;
            case 3 :
                TabFragment3 tab4 = new TabFragment3();
                return tab4;
            case 4:
                TabFragment5 tab5 = new TabFragment5();
                return tab5;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
