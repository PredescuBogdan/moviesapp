package bogdan.moviesapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import bogdan.moviesapp.R;
import bogdan.moviesapp.model.Cinema;
import bogdan.moviesapp.model.Movie;

/**
 * Created by Bogdan on 1/24/2017.
 */

public class PremiuDetailsMovieAdapter extends BaseAdapter{

    private Context context;
    private List<Movie> listaFilme;

    public PremiuDetailsMovieAdapter(Context context, List<Movie> data) {
        this.context = context;
        this.listaFilme = data;
    }


    @Override
    public int getCount() {
        return listaFilme.size();
    }

    @Override
    public Movie getItem(int i) {
        return listaFilme.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Movie movie = listaFilme.get(i);

        if(view == null) {
            view = inflater.inflate(R.layout.premiu_details_item, viewGroup,false);
        }



        TextView movieDate = (TextView) view.findViewById(R.id.premiuDetailsDate);
        TextView movieName = (TextView) view.findViewById(R.id.premiuDetailsNume);

        movieDate.setText(movie.getRelease_date());
        movieName.setText(movie.getName());
        return view;
    }
}
