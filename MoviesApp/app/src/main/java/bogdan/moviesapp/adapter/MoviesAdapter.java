package bogdan.moviesapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bogdan.moviesapp.R;
import bogdan.moviesapp.model.Movie;

/**
 * Created by Bogdan on 1/19/2017.
 */

public class MoviesAdapter extends BaseAdapter implements Filterable{


    private Context context;
    private List<Movie> moviesList;
    private ValueFilter valueFilter;
    private List<Movie>tempList;
    private ArrayList<Movie> filterList;

    public MoviesAdapter(Context context, List<Movie> data){
        this.context = context;
        this.moviesList = data;
        tempList = moviesList;
    }


    @Override
    public int getCount() {
        return moviesList.size();
    }

    @Override
    public Movie getItem(int i) {
        return moviesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Movie measurement = moviesList.get(i);


        if(view == null) {
            view = inflater.inflate(R.layout.movie_item,viewGroup, false);
        }



        ImageView image = (ImageView)view.findViewById(R.id.imageNew);
        image.setVisibility(View.GONE);
        TextView tvDate = (TextView)view.findViewById(R.id.movieTitle);
        TextView tvTemperature = (TextView)view.findViewById(R.id.movieReleaseYear);

        tvDate.setText("Title: " + measurement.getName());
        tvTemperature.setText("Date : " + measurement.getRelease_date());

        return view;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {
            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            filterList=new ArrayList<Movie>();
            if(constraint!=null && constraint.length()>0){
                for(int i=0;i<moviesList.size();i++){
                    if((moviesList.get(i).getName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        String title = moviesList.get(i).getName();
                        String date = moviesList.get(i).getRelease_date();
                        Movie movie = new Movie(title,date);
                        filterList.add(movie);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else {
                results.count=tempList.size();
                results.values=tempList;
            }
            return results;
        }

        //Invoked in the UI thread to publish the filtering results in the user interface.
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            moviesList=(List<Movie>) results.values;
            notifyDataSetChanged();
        }
    }
}
