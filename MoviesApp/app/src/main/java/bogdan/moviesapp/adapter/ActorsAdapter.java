package bogdan.moviesapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bogdan.moviesapp.R;
import bogdan.moviesapp.model.Actor;

/**
 * Created by Bogdan on 1/16/2017.
 */

public class ActorsAdapter extends BaseAdapter implements Filterable {


    private Context context;
    private List<Actor> actorsList;
    private ValueFilter valueFilter;
    private List<Actor> tempList;
    private ArrayList<Actor> filterList;

    public ActorsAdapter(Context context, List<Actor> data) {
        this.context = context;
        this.actorsList = data;
        tempList = actorsList;
    }


    @Override
    public int getCount() {
        return actorsList.size();
    }

    @Override
    public Actor getItem(int i) {
        return actorsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Actor measurement = actorsList.get(i);


        if(view == null) {
            view = inflater.inflate(R.layout.actor_item,viewGroup, false);
        }


        TextView tvActorFirstName = (TextView) view.findViewById(R.id.actorsName);

        tvActorFirstName.setText(measurement.getFirstName() + " " + measurement.getLastName());
//        tvActorMovies.setText("Movies :" + measurement.getMoviesPlayed());

        return view;
    }


    @Override
    public Filter getFilter() {
        if(valueFilter==null) {
            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected Filter.FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            filterList=new ArrayList<>();
            if(constraint!=null && constraint.length()>0){
                for(int i=0;i<actorsList.size();i++){
                    if((actorsList.get(i).getFirstName().toUpperCase())
                            .contains(constraint.toString().toUpperCase()) || actorsList.get(i).getLastName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        String firstName = actorsList.get(i).getFirstName();
                        String lastName = actorsList.get(i).getLastName();
                        String moviesPlayed = actorsList.get(i).getMoviesPlayed();
                        Actor actor = new Actor(firstName,lastName,moviesPlayed);
                        filterList.add(actor);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else {
                results.count=tempList.size();
                results.values=tempList;
            }
            return results;
        }

        //Invoked in the UI thread to publish the filtering results in the user interface.
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            actorsList=(List<Actor>) results.values;
            notifyDataSetChanged();
        }
    }
}