package bogdan.moviesapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import bogdan.moviesapp.R;
import bogdan.moviesapp.model.Cinema;
import bogdan.moviesapp.model.Premiu;

/**
 * Created by Bogdan on 1/24/2017.
 */

public class CinemaAdapter extends BaseAdapter{

    private Context context;
    private List<Cinema> listaCinematografe;

    public CinemaAdapter(Context context, List<Cinema> data) {
        this.context = context;
        this.listaCinematografe = data;
    }


    @Override
    public int getCount() {
        return listaCinematografe.size();
    }

    @Override
    public Cinema getItem(int i) {
        return listaCinematografe.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Cinema cinema = listaCinematografe.get(i);

        if(view == null) {
            view = inflater.inflate(R.layout.cinema_item, viewGroup,false);
        }



        TextView cinemaName = (TextView) view.findViewById(R.id.cinemaName);

        cinemaName.setText(cinema.getNume());
        return view;
    }
}
