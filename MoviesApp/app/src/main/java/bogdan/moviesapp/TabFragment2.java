package bogdan.moviesapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import bogdan.moviesapp.adapter.ActorsAdapter;
import bogdan.moviesapp.http.HttpHandler;
import bogdan.moviesapp.model.Actor;
import bogdan.moviesapp.util.JSONService;
import bogdan.moviesapp.util.iConstants;

public class TabFragment2 extends Fragment {
    private ListView listaActori;
    private EditText searchMessage;
    Context context;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaActori = (ListView) view.findViewById(R.id.listViewActori);
        searchMessage = (EditText) view.findViewById(R.id.inputSearchActori);
        context = view.getContext();
        IncarcatorDeJsoane incarcatorDeJsoane = new IncarcatorDeJsoane();
        incarcatorDeJsoane.execute();

    }



    private class IncarcatorDeJsoane extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();
            String json = handler.makeServiceCall(iConstants.SERVER_URL+"/getAllActorsWithMovies");
            return json;
        }



        @Override
        protected void onPostExecute(String s) {
            final List<Actor> movies = JSONService.actorsToList(s);
            final ActorsAdapter adaptor = new ActorsAdapter(context, movies);
            listaActori.setAdapter(adaptor);
            listaActori.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent actorsDetails = new Intent(getContext(), ActorsDetailsActivity.class);
                    actorsDetails.putExtra("Position", i);
                    startActivity(actorsDetails);
                }
            });

            searchMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    adaptor.getFilter().filter(charSequence);


                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            super.onPostExecute(s);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.tab_fragment_2, container, false);
    }
}
