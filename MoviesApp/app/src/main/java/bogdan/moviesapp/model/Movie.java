package bogdan.moviesapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
	private String name;
	private String release_date;
	private String description;
	private String actors;
	private String cover;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(name);
		parcel.writeString(release_date);
		parcel.writeString(description);
		parcel.writeString(actors);
		parcel.writeString(cover);
	}

	public Movie(){}

	public Movie(Parcel in){
		this.name = in.readString();
		this.release_date = in.readString();
		this.description = in.readString();
		this.actors = in.readString();
		this.cover = in.readString();
	}

	// this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
	public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
		public Movie createFromParcel(Parcel in) {
			return new Movie(in);
		}

		public Movie[] newArray(int size) {
			return new Movie[size];
		}
	};

	public Movie(String name, String release_date) {
		this.name = name;
		this.release_date = release_date;
	}

	public Movie(String name, String release_date, String description, String actors) {
		this.name = name;
		this.release_date = release_date;
		this.description = description;
		this.actors = actors;
	}

	public Movie(String name, String release_date, String description, String actors, String cover) {
		this.name = name;
		this.release_date = release_date;
		this.description = description;
		this.actors = actors;
		this.cover = cover;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRelease_date() {
		return release_date;
	}
	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}


}
