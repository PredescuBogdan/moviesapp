package bogdan.moviesapp.model;

import java.math.BigInteger;
import java.util.Date;


public class User {
	
	private Integer id;
	
	private String email;
	
	private Date date_added;

	private String password;

	private BigInteger id_facebook;

	public User() {
		
	}

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }


    public User(String email, Date date_added) {
		super();
		this.email = email;
		this.date_added = date_added;
	}


	public User(BigInteger id_facebook, String email) {
		this.id_facebook = id_facebook;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDate_added() {
		return date_added;
	}

	public void setDate_added(Date date_added) {
		this.date_added = date_added;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public BigInteger getId_facebook() {
		return id_facebook;
	}

	public void setId_facebook(BigInteger id_facebook) {
		this.id_facebook = id_facebook;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", email='" + email + '\'' +
				", date_added=" + date_added +
				", password='" + password + '\'' +
				", id_facebook=" + id_facebook +
				'}';
	}
}
