package bogdan.moviesapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {
	
	private String message;
	private boolean access;
	
	public Message() {
	}
	
	public Message(String message, boolean access) {
		super();
		this.message = message;
		this.access = access;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isAccess() {
		return access;
	}
	public void setAccess(boolean access) {
		this.access = access;
	}
	
	
	
}
