package bogdan.moviesapp.model;

/**
 * Created by Bogdan on 1/24/2017.
 */

public class Premiu {
    private String nume;

    public Premiu(String nume) {
        super();
        this.nume = nume;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }


}