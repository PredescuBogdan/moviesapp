package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_fb")
public class UserFacebook {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Integer id;
	
	@Column(name = "fb_user_id")
	private Integer fb_user_id;
	
	public UserFacebook() {
	}

	public UserFacebook(Integer fb_user_id) {
		this.fb_user_id = fb_user_id;
	}

	public Integer getFb_user_id() {
		return fb_user_id;
	}

	public void setFb_user_id(Integer fb_user_id) {
		this.fb_user_id = fb_user_id;
	}
}
