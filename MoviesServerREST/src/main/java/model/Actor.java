package model;

public class Actor {
	private String firstName;
	private String lastName;
	private String moviesPlayed;
	
	public Actor(String firstName, String lastName, String moviesPlayed) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.moviesPlayed = moviesPlayed;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMoviesPlayed() {
		return moviesPlayed;
	}
	public void setMoviesPlayed(String moviesPlayed) {
		this.moviesPlayed = moviesPlayed;
	}
	
	
	
}
