package model;

public class Premiu {
	private String nume;

	public Premiu(String nume) {
		super();
		this.nume = nume;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	@Override
	public String toString() {
		return "Premiu [nume=" + nume + "]";
	}

	
}
