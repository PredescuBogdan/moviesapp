package model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table (name="users")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private Integer id;
	
	@Column(name="email", unique = true)
	private String email;
	
	@Column(name = "date_added", columnDefinition="DATETIME")
	private Date date_added;
	
	@Transient
	private String password;
	
	@Transient
	private BigInteger id_facebook;
	
	public User() {
		
	}

	public User(String email) {
		super();
		this.email = email;
	}

	public User(String email, Date date_added) {
		this.email = email;
		this.date_added = date_added;
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	
	public User(BigInteger id_facebook, String email) {
		this.id_facebook = id_facebook;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDate_added() {
		return date_added.toString();
	}

	public void setDate_added(Date date_added) {
		this.date_added = date_added;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigInteger getId_facebook() {
		return id_facebook;
	}

	public void setId_facebook(BigInteger id_facebook) {
		this.id_facebook = id_facebook;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", date_added=" + date_added + ", password=" + password + "]";
	}
}
