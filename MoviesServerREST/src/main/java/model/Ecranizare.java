package model;

public class Ecranizare {
	private String releaseDate;
	private String details;
	private String title;
	private String cinemaName;
	public Ecranizare(String releaseDate, String details, String title, String cinemaName) {
		super();
		this.releaseDate = releaseDate;
		this.details = details;
		this.title = title;
		this.cinemaName = cinemaName;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	
	
}
