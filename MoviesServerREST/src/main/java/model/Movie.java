package model;

public class Movie {
	public String name;
	public String release_date;
	public String description;
	public String actors;
	public String cover;
	
	public Movie(String name) {
		this.name = name;
	}
	
	public Movie(String name, String release_date) {
		this.name = name;
		this.release_date = release_date;
	}
	
	public Movie(String name, String release_date, String description, String actors) {
		this.name = name;
		this.release_date = release_date;
		this.description = description;
		this.actors = actors;
	}
	
	public Movie(String name, String release_date, String description, String actors, String cover) {
		this.name = name;
		this.release_date = release_date;
		this.description = description;
		this.actors = actors;
		this.cover = cover;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRelease_date() {
		return release_date;
	}
	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	@Override
	public String toString() {
		return "Movie [name=" + name + "]";
	}

	
	
	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}
	
	
}
