package model;

public class Trailer {
	private int id;
	private String trailerLink;
	
	public Trailer(int id, String trailerLink) {
		super();
		this.id = id;
		this.trailerLink = trailerLink;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTrailerLink() {
		return trailerLink;
	}
	public void setTrailerLink(String trailerLink) {
		this.trailerLink = trailerLink;
	}
	
	
}
