package ro.app.dao;

import model.UserFacebook;

public class DAOUserFacebook extends GenericDaoHibernateImpl<UserFacebook, Integer>{
	public DAOUserFacebook() {
		super(UserFacebook.class);
	}
}
