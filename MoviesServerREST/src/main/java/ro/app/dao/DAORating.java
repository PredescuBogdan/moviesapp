package ro.app.dao;

import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import model.Rating;


public class DAORating {
	
	public void updateRating(double rating, int user_id, int film_id) {
		Statement stmt;
		try{
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();
			
			String sql = "update filme_ratings set rating = " + rating +" where id_user = " + user_id + " and id_film = " + film_id;
			System.out.println(sql);
			stmt.execute(sql);
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertRating(int user_id, int film_id, double rating) {
		Statement stmt;
		try{
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();
			
			String sql = "insert into filme_ratings (id_user,id_film,rating) VALUES (" + user_id +", " + film_id + ", " + rating + ");";
			stmt.execute(sql);
			
			conn.close();
		}catch(MySQLIntegrityConstraintViolationException e) {
			System.out.println("Rating for movie existing in db, updating instead");
			updateRating(rating, user_id, film_id);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void checkRating(Rating r){
		Statement stmt;
		int user_id = 0;
		int film_id = 0;
		try{
			stmt = (Statement) service.DBConnection.getConnection().createStatement();
			
			String sql = "select (select user_id from users where email = '" + r.getEmail() +"')AS user_id, " 
						+"(select film_id from filme where title = '" + r.getTitle() + "') AS film_id;"; 
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				user_id = rs.getInt("user_id");
				film_id = rs.getInt("film_id");
				insertRating(user_id, film_id, r.getRating());
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Double getRatingForMovie(String title) {
		
		double rating = 0;
		Statement stmt;
		try{
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();
			
			String sql = "select AVG (filme_ratings.rating) as ratingAVG from filme_ratings "
					+ "inner join filme on filme.film_id = filme_ratings.id_film where title = '" + title +"'";
			System.out.println(sql);
			stmt.execute(sql);
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()) {
				rating = rs.getDouble("ratingAVG");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return rating;
	}
}
