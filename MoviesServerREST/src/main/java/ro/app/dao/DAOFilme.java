package ro.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Movie;

public class DAOFilme {

	public List<Movie> findAllMovies(int limit) {
		List<Movie> allMovies = new ArrayList<Movie>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select filme.title, filme.release_year, filme.cover from filme ORDER BY last_update LIMIT "
					+ limit;
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Movie m = new Movie(rs.getString("title"), rs.getString("release_year"));
				m.setCover(rs.getString("cover"));
				allMovies.add(m);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allMovies;
	}

	public List<Movie> findAllMoviesWithActors(int limit) {
		List<Movie> allMovies = new ArrayList<Movie>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select filme.title, filme. release_year, filme.description, "
					+ "group_concat(actori.first_name, SPACE(1), actori.last_name SEPARATOR ',') as Actori, filme.cover, "
					+ "filme.last_update "
					+ "from filme inner join actori_filme on filme.film_id = actori_filme.film_id "
					+ "inner join actori on actori_filme.actor_id = actori.actor_id GROUP BY title ORDER BY last_update limit "+ limit;
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Movie m = new Movie(rs.getString("title"), rs.getString("release_year"), rs.getString("description"),
						rs.getString("Actori"), rs.getString("cover"));
				allMovies.add(m);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allMovies;
	}

	public Movie findMovieWithActorsByFilmId(int film_id) {
		Statement stmt;
		Movie m = null;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select filme.film_id, filme.title, filme. release_year, filme.description, "
					+ "group_concat(actori.first_name, SPACE(1), actori.last_name SEPARATOR ',') as Actori, filme.cover, "
					+ "filme.last_update "
					+ "from filme inner join actori_filme on filme.film_id = actori_filme.film_id "
					+ "inner join actori on actori_filme.actor_id = actori.actor_id  WHERE filme.film_id = " + film_id
					+ " GROUP BY title ORDER BY last_update";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				m = new Movie(rs.getString("title"), rs.getString("release_year"), rs.getString("description"),
						rs.getString("Actori"), rs.getString("cover"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return m;
	}

	public String findCoverByMovie(String title) {
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();


			PreparedStatement stmt = conn.prepareStatement("select filme.cover from filme where title = ?");
			stmt.setString(1, title);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return rs.getString("cover");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public String findTrailerByMovie(String title) {
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();

			
			String sql = "select trailerLink from trailere inner join filme_trailere on filme_trailere.trailer_id = trailere.id "
					+ "inner join filme on filme.film_id = filme_trailere.film_id where title = ?";
			
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, title);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return rs.getString("trailerLink");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
