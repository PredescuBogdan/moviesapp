package ro.app.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import model.Actor;
import model.Movie;
import model.Premiu;

public class DAOActor {

//	public static void main(String[] args) {
//		DAOActor dao = new DAOActor();
//		Map<String, List<Movie>> moviz = dao.findAllByPremiu();
//		for (Entry<String, List<Movie>> entry : moviz.entrySet()) {
//			System.out.println(entry.getKey() + "/" + entry.getValue());
//		}
//	}

	public Map<Premiu, List<Movie>> findAllByPremiu() {
		Map<Premiu, List<Movie>> moviesAndPremiis = new HashMap<>();
		List<Premiu> premii = findTipPremii();
		for (Premiu premiu : premii) {
			if (moviesAndPremiis.containsKey(premiu)) {
				if (moviesAndPremiis.get(premiu) == null) {
					moviesAndPremiis.put(premiu, new ArrayList<>());
				}
			} else {
				moviesAndPremiis.put(premiu, new ArrayList<>());

			}
			moviesAndPremiis.get(premiu).addAll(findAllMoviesForPremiu(premiu));

		}
		return moviesAndPremiis;
	}

	public List<Premiu> findTipPremii() {
		List<Premiu> premii = new ArrayList<>();
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			Statement stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "SELECT * FROM tip_premiu";

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				premii.add(new Premiu(rs.getString("nume_tip")));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return premii;
	}

	public List<Movie> findAllMoviesForPremiu(Premiu premiu) {
		List<Movie> allMovies = new ArrayList<Movie>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select filme.title, filme_premii.data_acordare AS data_acordare from filme inner join filme_premii on filme_premii.id_film = filme.film_id inner join tip_premiu on tip_premiu.id = filme_premii.id_tip_premiu where tip_premiu.nume_tip = '"
					+ premiu.getNume() + "'";

			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Movie movie = new Movie(rs.getString("title"), rs.getString("data_acordare"));
				allMovies.add(movie);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allMovies;
	}

	public List<Actor> findAllActorWithMovies(int limit) {
		List<Actor> allActors = new ArrayList<Actor>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select first_name, last_name, " + "group_concat(title SEPARATOR ', ') as Filme "
					+ "from actori " + "inner join actori_filme on actori.actor_id = actori_filme.actor_id "
					+ "inner join filme on actori_filme.film_id = filme.film_id " + "group by first_name limit " + limit
					+ ";";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Actor actor = new Actor(rs.getString("first_name"), rs.getString("last_name"), rs.getString("Filme"));
				allActors.add(actor);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allActors;
	}

}
