package ro.app.dao;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import model.User;

public class DAOUser extends GenericDaoHibernateImpl<User, String> {

	public DAOUser() {
		super(User.class);
	}

	public List<User> findAllUserNormal() {
		List<User> allUsers = new ArrayList<User>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select users.email, user_normal.password, users.date_added from users "
					+ "inner join user_normal on users.user_id = user_normal.user_id;";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				User u = new User(rs.getString("email"), rs.getString("password"));
				allUsers.add(u);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allUsers;
	}

	public List<User> findAllUserFacebook() {
		List<User> allUsers = new ArrayList<User>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select user_fb.fb_user_id, users.email from user_fb "
					+ "inner join users on user_fb.user_id = users.user_id;";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				User u = new User(rs.getString("fb_user_id"), rs.getString("email"));
				allUsers.add(u);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allUsers;
	}

	public User findUserNormal(User userCautat) {
		Statement stmt;
		User u = null;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select users.email, user_normal.password from users "
					+ "inner join user_normal on users.user_id = user_normal.user_id where email = '"
					+ userCautat.getEmail() + "' " + "and password = '" + userCautat.getPassword() + "';";
			ResultSet rs = stmt.executeQuery(sql);

			System.out.println(sql);

			while (rs.next()) {
				u = new User(rs.getString("email"), rs.getString("password"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}

	public User returnUserByCriteria(String criteria, String field) {
		User u = null;
		Criteria cr = getSession().createCriteria(User.class);
		cr.add(Restrictions.eq(criteria, field));
		List results = cr.list();
		for (Iterator<User> iterator = results.iterator(); iterator.hasNext();) {
			u = iterator.next();
		}
		return u;
	}

	public User findUserFacebook(User userCautat) {
		Statement stmt;
		User u = null;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select user_fb.fb_user_id, users.email from user_fb "
					+ "inner join users on user_fb.user_id = users.user_id where email = '" + userCautat.getEmail()
					+ "' " + "and fb_user_id = '" + userCautat.getId_facebook() + "';";
			ResultSet rs = stmt.executeQuery(sql);

			System.out.println(sql);

			while (rs.next()) {
				u = new User(new BigInteger(rs.getString("fb_user_id")), rs.getString("email"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}
}
