package ro.app.dao;

import model.UserNormal;

public class DAOUserNormal extends GenericDaoHibernateImpl<UserNormal, Integer>{
	public DAOUserNormal() {
		super(UserNormal.class);
	}
}
