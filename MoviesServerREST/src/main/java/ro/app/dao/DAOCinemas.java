package ro.app.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Cinema;
import model.Ecranizare;

public class DAOCinemas {
	public List<Cinema> findAllCinemas(int limit) {
		List<Cinema> allCinemas = new ArrayList<Cinema>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select * from cinematografe LIMIT " + limit;
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Cinema cinema = new Cinema(rs.getInt("id"), rs.getString("nume"), rs.getString("adresa"));
				allCinemas.add(cinema);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allCinemas;
	}
	
	public List<Ecranizare> findAllEcranizariForMovie(String movie) {
		List<Ecranizare> allEcranizari = new ArrayList<Ecranizare>();
		Statement stmt;
		try {
			Connection conn = (Connection) service.DBConnection.getConnection();
			stmt = (Statement) service.DBConnection.getConnection().createStatement();

			String sql = "select data_ecranizare AS Data,  detalii as Detalii, "
 + "filme.title, cinematografe.nume from ecranizari inner join filme on filme.film_id = ecranizari.film_id "
 + "inner join cinematografe on cinematografe.id = ecranizari.cinema_id where title = '" +movie + "' group by data";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Ecranizare ecranizare = new Ecranizare(rs.getString("Data"), rs.getString("Detalii"), rs.getString("title"), rs.getString("nume"));
				allEcranizari.add(ecranizare);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return allEcranizari;
	}
}
