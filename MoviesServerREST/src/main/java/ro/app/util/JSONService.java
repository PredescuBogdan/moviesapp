package ro.app.util;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Actor;
import model.Movie;
import model.User;


public class JSONService {
	
	public static String user2JSON(User user){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	public static String usersToJSON(List<User> users){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(User user : users){
			if(index==users.size()-1){
				sb.append(user2JSON(user));
			}else{
				sb.append(user2JSON(user)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}


	public static String movie2JSON(Movie movie){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(movie);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}

	public static String moviesToJSON(List<Movie> movies){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(Movie movie : movies){
			if(index==movies.size()-1){
				sb.append(movie2JSON(movie));
			}else{
				sb.append(movie2JSON(movie)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}

	public static String actor2JSON(Actor actor){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(actor);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	public static String actorsToJSON(List<Actor> actors){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int index = 0;
		for(Actor actor : actors){
			if(index==actors.size()-1){
				sb.append(actor2JSON(actor));
			}else{
				sb.append(actor2JSON(actor)).append(",");
			}
			index++;
		}
		sb.append("]");
		return sb.toString();
	}
	
}
