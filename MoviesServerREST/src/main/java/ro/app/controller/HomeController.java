package ro.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/navigare")
public class HomeController {


	@RequestMapping("/acasa")
	public ModelAndView daOPagina(){
		ModelAndView mav = new ModelAndView("acasa");
		return mav;
	}
}
