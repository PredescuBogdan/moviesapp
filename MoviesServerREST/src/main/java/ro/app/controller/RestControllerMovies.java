package ro.app.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.Actor;
import model.Message;
import model.Movie;
import model.Premiu;
import model.Rating;
import model.User;
import ro.app.dao.DAOActor;
import ro.app.dao.DAOFilme;
import ro.app.dao.DAORating;
import ro.app.dao.DAOUser;
import ro.app.util.JSONService;

@RestController
public class RestControllerMovies {
	
	private DAOUser daoUser = new DAOUser();
	private DAOFilme daoFilm = new DAOFilme();
	private DAOActor daoActor = new DAOActor();
	private DAORating daoRating = new DAORating();
	
	@RequestMapping(value= "/test", produces = MediaType.TEXT_HTML_VALUE, method = RequestMethod.GET) 
	public String returnReviews(@RequestParam("user") String user) {
		if(user.equals("Bogdan")) {
			return "Succes";
		}else {
			return "Failed";
		}
	}
	
	@RequestMapping(value= "/findUser", method = RequestMethod.POST)
	public ResponseEntity<Message> getAccessUser(@RequestBody User userCautat) {
		DAOUser daoUser = new DAOUser();
		List<User>allUsers = daoUser.findAll();
		List<User>allUsersNormal = new DAOUser().findAllUserNormal();
		Message message = null;
		
		for(User u : allUsers) {

			System.out.println(u.getEmail() + " comparat cu " + userCautat.getEmail());
			if(u.getEmail().equals(userCautat.getEmail())) {
				
				
				message = new Message("L-am gasit", true);
				System.out.println("L-am gasit");
			}
			
			else { 
				System.out.println("Nu exista");
				message = new Message("Nu l-am gasit", false);
				
			}
		}
		
		System.out.println("TOTI USERII NORMALI ");
		for(User uNormal : allUsersNormal) {
			System.out.println(uNormal.getEmail() + " " + uNormal.getPassword());
		}
		return new ResponseEntity<Message>(message,HttpStatus.CREATED);
	} 
	
	@RequestMapping(value= "/getAllUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getAllNormalUsers() {
		List<User>allUsers = daoUser.findAll();
		return JSONService.usersToJSON(allUsers); 
	} 

	@RequestMapping(value= "/findPremii", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Premiu> getPremii() {
		List<Premiu> m1 = daoActor.findTipPremii();
		return m1;
	} 
	
	
	@RequestMapping(value= "/findAllMoviesByPremiu/{premiu}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Movie> getMoviesByPremiu(@PathVariable (value = "premiu") String premiu) {
		Premiu prem = new Premiu(premiu);
		List<Movie> m1 = daoActor.findAllMoviesForPremiu(prem);
		return m1;
	} 
	
	@RequestMapping(value= "/getAllMoviesWithActors/{limit}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Movie> getAllMoviesWithActors(@PathVariable (value = "limit")int limit) {
		List<Movie>allMovies = daoFilm.findAllMoviesWithActors(limit);
		return allMovies;
	} 
	
	@RequestMapping(value= "/findMovieWithActorsByTitle/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Movie getMovieByTitle(@PathVariable (value = "id") Integer id) {
		Movie m = daoFilm.findMovieWithActorsByFilmId(id);
		return m;
	} 
	
	@RequestMapping(value= "/getAllMovies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getAllMovies() {
		List<Movie>allMovies = daoFilm.findAllMovies(20);
		return JSONService.moviesToJSON(allMovies); 
	} 
	
	@RequestMapping(value= "/getAllMovies/{limit}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getAllMoviesWithLimit(@PathVariable (value = "limit")Integer limit) {
		List<Movie>allMovies = daoFilm.findAllMovies(limit);
		return JSONService.moviesToJSON(allMovies); 
	} 
	
	
	@RequestMapping(value= "/getAllActorsWithMovies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getAllActorsWithMovies() {
		List<Actor>allActors = daoActor.findAllActorWithMovies(200);
		return JSONService.actorsToJSON(allActors); 
	} 
	
	@RequestMapping(value= "/getCoverByMovie/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getCoverForMovie(@PathVariable (value = "title")String title) {
		String cover = daoFilm.findCoverByMovie(title.replace("'", "\\'"));
		return cover; 
	} 
	
	@RequestMapping(value= "/getTrailerByMovie/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getTrailerByMovie(@PathVariable (value = "title")String title) {
		String cover = daoFilm.findTrailerByMovie(title.replace("'", "\\'"));
		System.out.println("TITLE MOV:" + title);
		return cover; 
	} 
	
	
	@RequestMapping(value="/putdata/{email_user}/{film_title}", method = RequestMethod.PUT)
	public void putData(@PathVariable(value = "email_user") String email_user,
			           @PathVariable(value = "film_title") String film_title,
			           @RequestParam(value = "rating")String rating)
						 {
		
//		Rating r = new Rating(user_id,film_id,user_rating);
		daoRating.checkRating(new Rating(email_user,film_title,Double.valueOf(rating)));
		
	} 
	
	@RequestMapping(value= "/findUserFacebook", method = RequestMethod.POST)
	public ResponseEntity<Message> getAccessUserFacebook(@RequestBody User userCautat) {
		Message message = null;
		try{
			User u = daoUser.findUserFacebook(userCautat);
			if(u!= null) {
				message = new Message("Login Successfully", true);
			} else {
				message = new Message("Login Failed", false);
			}
		}catch(Exception e) {
			message = new Message("Aplication Problem", false);
			e.printStackTrace();
		}
		return new ResponseEntity<Message>(message,HttpStatus.CREATED);
	} 
	
	@RequestMapping(value= "/findNormalUser", method = RequestMethod.POST)
	public ResponseEntity<Message> getAccessUserNormalOne(@RequestBody User userCautat) {
		Message message = null;
		try{
			User u = daoUser.findUserNormal(userCautat);
			if(u!= null) {
				message = new Message("Login Successfully", true);
			} else {
				message = new Message("Login Failed", false);
			}
		}catch(Exception e) {
			message = new Message("Aplication Problem", false);
			e.printStackTrace();
		}
		return new ResponseEntity<Message>(message,HttpStatus.CREATED);
	} 

	

	@RequestMapping(value= "/getRatingForMovie/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getRatingForMovie(@PathVariable (value = "title")String title) {
		Double rating = daoRating.getRatingForMovie(title.replace("'", "\'"));
		System.out.println("RATING MOV:" + rating);
		return String.valueOf(rating);
	} 
}
