package ro.app.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Cinema;
import model.Ecranizare;
import ro.app.dao.DAOCinemas;

@RestController
@RequestMapping("/cinematografe")
public class RestControllerCinemas {

	private DAOCinemas dao = new DAOCinemas();
	@RequestMapping("/all")
	public List<Cinema> findAll(){
		List<Cinema> cinemas = dao.findAllCinemas(10);
		return cinemas;
	}
	
	@RequestMapping("/ecranizareMovie/{movieName}")
	public List<Ecranizare> findAllForMovie(@PathVariable (value = "movieName") String movieName) {
		List<Ecranizare> ecranizari = dao.findAllEcranizariForMovie(movieName.replace("'", "\\'"));
		return ecranizari;
	}
}
